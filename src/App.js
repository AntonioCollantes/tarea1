import React, { Component } from 'react';
import Header from './components/Header';
import Tablero from './components/Tablero';
import baraja from './utils/baraja';
import mensaje from './components/AlertPopup';

const estadoInicial = {
  baraja: baraja(),
  parejaSeleccionada: [], // entran 2 cartas
  estamosComparando: false,
  numeroDeIntentos: 0,
};

export default class App extends Component {

  state = {
    ...estadoInicial,
  };

  seleccionarCarta = (carta) => {
    console.log('miremos la carta');
    console.log(carta);

    /**
     * Validamos 3 casos
     * Si estamos comparando no volteamos otras cartas
     * si la carta fue adivinada, no volvemos a volterar
     * si la carta seleccionada, esta en arreglo comparador tampoco lo volteamos
     */
    if (
      this.state.estamosComparando ||
      carta.fueAdivinada ||
      this.state.parejaSeleccionada.indexOf(carta) > -1
    ) {
      return;
    }

    const parejaSeleccionada = [...this.state.parejaSeleccionada, carta];

    this.setState({ parejaSeleccionada: parejaSeleccionada });

    if (parejaSeleccionada.length === 2) {
      this.compararPareja(parejaSeleccionada);
    }
  };

  compararPareja = (arregloComparador) => {
    // this.setState({ ...this.state, estamosComparando: true });
    this.setState((prevState) => ({ ...prevState, estamosComparando: true }));

    setTimeout(() => {
      const [primerCarta, segundaCarta] = arregloComparador;
      let baraja = this.state.baraja;
      if (primerCarta.icono === segundaCarta.icono) {
        baraja = baraja.map((carta) => {
          if (carta.icono !== primerCarta.icono) {
            return carta;
          }
          return { ...carta, fueAdivinada: true };
        });
      }

      this.verificamosGanador(baraja);

      this.setState({
        baraja: baraja,
        estamosComparando: false,
        parejaSeleccionada: [],
        numeroDeIntentos: this.state.numeroDeIntentos + 1,
      });
    }, 1000);

  };

  reiniciarJuego = () => {
    this.setState = {
      ...estadoInicial,
    };
  }

  verificamosGanador = (barajaActualizada) => {
    //recorro el arreglo y a todo lo asigno q fue adivinado
    /**barajaActualizada.forEach(carta => {
      carta.fueAdivinada = true;
    });*/

    if(barajaActualizada.filter((carta) => !carta.fueAdivinada).length === 0) {
      mensaje("Felicitaciones Ud. Gano el juego en " + this.state.numeroDeIntentos +" intentos.");
      //alert("Felicitaciones Ud. Gano el juego en " + this.state.numeroDeIntentos +" intentos.");
    }
  }

  render() {
    return (
      <div className='container'>
        <Header 
          numeroDeIntentos = {this.state.numeroDeIntentos}
          reiniciarJuego = {() => this.reiniciarJuego()}
        />
        <Tablero
          baraja={this.state.baraja}
          seleccionarCarta={this.seleccionarCarta}
          parejaSeleccionada={this.state.parejaSeleccionada}
        />
      </div>
    );
  }
}
