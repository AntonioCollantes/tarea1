import shuffle from 'lodash.shuffle';
import { listado } from './fontAwesomeClass';
const NUMERO_CARTAS = 20;

const baraja = () => {
  const iconosFilanes = listado();
  let cartas = [];

  while (cartas.length < NUMERO_CARTAS) {
    const indice = Math.floor(Math.random() * iconosFilanes.length);

    const carta = {
      icono: iconosFilanes.splice(indice, 1)[0],
      fueAdivinada: false,
    };

    cartas.push(carta);
    cartas.push({ ...carta });

  }
  return shuffle(cartas);
};

export default baraja;
