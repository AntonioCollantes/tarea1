import React from 'react';
import Carta from './Carta';
const Tablero = ({ baraja, seleccionarCarta, parejaSeleccionada }) => {
  console.log(baraja);
  return (
    <div
      className='d-flex justify-content-between flex-wrap tablero-container'
      style={{ width: '46em', margin: '0 auto' }}
    >
      {baraja.map((item, index) => {
        const estaSiendoComparada = parejaSeleccionada.indexOf(item) > -1;
        return (
          <Carta
            key={index}
            info={item}
            estaSiendoComparada={estaSiendoComparada}
            seleccionarCarta={() => seleccionarCarta(item)}
          />
        );
      })}
    </div>
  );
};

export default Tablero;
