import React, { Component } from 'react';

class Header extends Component {

  render() {
    return (
      <nav
        className='navbar navbar-expand-lg navbar-dark bg-dark'
        style={{ padding: '0 1em' }}
      >
        <p className='navbar-brand'>Juego de Cartas</p>
  
        <div className='navbar-collapse'>
          <h3 className='m-auto text-white'>Intentos: {this.props.numeroDeIntentos}</h3>
          <form className='form-inline my-2 my-lg-0'>
            <button type='submit' onClick= {this.props.reiniciarJuego} className='btn btn-light'>Reiniciar</button>
          </form>
        </div>
      </nav>
    );
  }

}
/**const Header = ({reiniciar,intentos}) => {
  return (
    <nav
      className='navbar navbar-expand-lg navbar-dark bg-dark'
      style={{ padding: '0 1em' }}
    >
      <p className='navbar-brand'>Juego de Cartas</p>

      <div className='navbar-collapse'>
        <h3 className='m-auto text-white'>Intentos: {intentos}</h3>
        <form onSubmit={this.handleSubmit} className='form-inline my-2 my-lg-0'>
          <button type='submit' className='btn btn-light'>Reiniciar</button>
        </form>
      </div>
    </nav>
  );
};*/

export default Header;
