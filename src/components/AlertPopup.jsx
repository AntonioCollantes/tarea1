import Swal from 'sweetalert2'

const mensaje = (msg) => {
    Swal.fire({
        position: 'top',
        icon: 'success',
        title: msg,
        showConfirmButton: false,
        timer: 3500
      })
}

export default mensaje;