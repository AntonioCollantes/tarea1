import React, { Component } from 'react';
import ReactCardFlip from 'react-card-flip';

import { stilos } from '../style/carta';

export default class Carta extends Component {
  state = {
    isFlipped: false,
  };

  render() {
    const { icono, fueAdivinada } = this.props.info;
    const { seleccionarCarta, estaSiendoComparada } = this.props;

    const mostrarCarta = fueAdivinada || estaSiendoComparada;
    return (
      <div className='card my-2' style={stilos.carta}>
        <ReactCardFlip
          isFlipped={mostrarCarta}
          flipDirection='horizontal'
        >
          <div
            onClick={seleccionarCarta}
            className='card-body p-0'
            style={stilos.backCard}
          >
            <p className='card-text'>
              <small>React</small>
            </p>
          </div>

          <div
            className='card-body p-0'
            style={stilos.frontCard}
          >
            <p className='card-text'>
              <i className={`fa ${icono}`}></i>
            </p>
          </div>
        </ReactCardFlip>
      </div>
    );
  }
}
