export const stilos = {
  carta: {
    margin: '0 auto',
    width: '125px',
    height: '125px',
    textAlign: 'center',
  },

  backCard: {
    backgroundColor: '#ffb300',
    width: '125px',
    height: '125px',
    margin: '0 auto',
    borderRadius: '5px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
    fontSize: '2em',
  },
  
  frontCard: {
    backgroundColor: '#03dcf4',
    width: '125px',
    height: '125px',
    margin: '0 auto',
    borderRadius: '5px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
    fontSize: '2em',
  },
};
